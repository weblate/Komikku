# <a href="https://apps.gnome.org/app/info.febvre.Komikku/"><img height="88" src="data/icons/info.febvre.Komikku.svg" />Komikku</a>

[![](https://circle.gnome.org/assets/button/badge.svg)](https://apps.gnome.org/app/info.febvre.Komikku/)
[![](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app)
[![Pipeline status](https://gitlab.com/valos/Komikku/badges/main/pipeline.svg)](https://gitlab.com/valos/Komikku/-/pipelines)
[![Donate using Liberapay](https://img.shields.io/liberapay/receives/valos.svg?logo=liberapay)](https://en.liberapay.com/valos/donate)

__Komikku__ is a manga reader for [GNOME](https://www.gnome.org). It focuses on providing a clean, intuitive and adaptive interface.

## License

__Komikku__ is licensed under the [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html).

## Keys features

* Online reading from dozens of servers
* Offline reading of downloaded comics
* Support for locally stored comics (in CBZ or CBR formats)
* RTL, LTR, Vertical and Webtoon reading modes
* Several types of navigation:
  * Keyboard arrow keys
  * Right and left navigation layout via mouse click or tapping (touchpad/touch screen)
  * Mouse wheel
  * 2-fingers swipe gesture (touchpad)
  * Swipe gesture (touch screen)
* Categories to organize your library
* Automatic update of comics
* Automatic download of new chapters
* Reading history
* Light and dark themes

## Screenshots

<img src="screenshots/library-dark.png" width="912">

## Installation

### Flatpak

<a href='https://flathub.org/apps/details/info.febvre.Komikku'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

### Native package

__Komikku__ is available as native package in the repositories of the following distributions:

[![Packaging status](https://repology.org/badge/vertical-allrepos/komikku.svg)](https://repology.org/project/komikku/versions)

### Flatpak of development version

1. Setup [Flatpak](https://www.flatpak.org/setup/) for your Linux distro.
2. Download the Komikku flatpak from the last passed [Gitlab pipeline](https://gitlab.com/valos/Komikku/-/jobs/artifacts/main/raw/info.febvre.Komikku.flatpak?job=flatpak).
3. Install the flatpak:
```bash
flatpak install info.febvre.Komikku.flatpak
```
4. Launch it:
```bash
flatpak run -d info.febvre.Komikku
```

## Contributing

### Code

Please follow our [contributing guidelines](CONTRIBUTING.md).

### Translations

Helping to translate __Komikku__ or add support to a new language is very welcome.

### Code of Conduct
We follow the [GNOME Code of Conduct](/CODE_OF_CONDUCT.md).
All communications in project spaces are expected to follow it.

## Sponsor this project

__Komikku__ is a `Free software`. If you like it and would like to support and fund it, you may donate through one of the plateform below. Any amount will be greatly appreciated __;-)__

|Plateforms|||
|:---:|:--:|---|
|Ko-fi|[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/X8X06EM3L)|One-time or monthly donation|
|Liberapay|[![lp Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/valos/donate)|Weekly/monthly/yearly donation|
|PayPal|[![PayPal](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/donate?business=GSRGEQ78V97PU&no_recurring=0&item_name=You+can+help+me+to+keep+developing+apps+through+donations.&currency_code=EUR)|One-time donation|

## Disclaimer

The developers of this application does not have any affiliation with the content providers available.
